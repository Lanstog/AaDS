import views.MainWindow;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() { //Инициализация работы, запуск главного окна
                MainWindow mainWindow = MainWindow.getObject();
                mainWindow.setVisible(true);
                System.exit(0);
            }
        });
    }
}