package models;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public abstract class DataBaseModel {
    private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/Labs";
    private static final String user = "postgres";
    private static final String password = "123";
    private static int countOfIteration=1;
    private static int id=1;
    private static Connection connectToDB() {
        //System.out.println("Testing connection to PostgreSQL JDBC");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return null;
        }

        //System.out.println("PostgreSQL JDBC Driver successfully connected");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_URL, user, password);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return null;
        }

        if (connection != null) {
            //System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }
        return connection;
    }

    public static int getResultElem(int index) {

        int number = -2147483648;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT result[" + (index) + "] FROM lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();

        } catch (Exception e) {
            System.out.println("Unexpected error!");
        }
        return number;
    }

    public static int getFirstPartElem(int index) {

        int number = -2147483648;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT firstpart[" + index + "] FROM lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();

        } catch (Exception e) {
            System.out.println("Unexpected error!");
        }
        return number;
    }

    public static int getSecondPartElem(int index) {

        int number = -2147483648;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT secondpart[" + index + "] FROM lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();

        } catch (Exception e) {
            System.out.println("Unexpected error!");
        }
        return number;
    }

    public static void sendArray(ArrayList<Integer> arrayList) {
        try {
            Connection connection = connectToDB();
            String sql = "INSERT INTO lab5 (startArray) values(?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            Array array = connection.createArrayOf("integer", arrayList.toArray());
            stmt.setArray(1, array);
            stmt.executeUpdate();
            id++;
            countOfIteration=1;
            String sql2= "INSERT INTO lab5iterations (result, id) select startarray, id from lab5 order by id desc limit 1";
            PreparedStatement stmt2 = connection.prepareStatement(sql2);
            stmt2.executeUpdate();
            String sql3= "update lab5iterations set countofiteration="+countOfIteration+" where countofiteration=-1";
            PreparedStatement stmt3 = connection.prepareStatement(sql3);
            stmt3.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка отправки массива!");
        }
    }

    public static int getResultLength() {
        int number = -1;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select array_length(result, 1) from lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
                //System.out.println(number);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения длины массива!");
        }
        return number;
    }

    public static int getFirstPartLength() {
        int number = -1;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select array_length(firstpart, 1) from lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения длины массива!");
        }
        return number;
    }

    public static int getSecondPartLength() {
        int number = -1;
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select array_length(secondpart, 1) from lab5iterations where id="+id+" and countOfIteration="+countOfIteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                number = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения длины массива!");
        }
        return number;
    }

    public static void setFirstPartArray(int number, int pos) {
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set firstpart[" + pos + "]=? where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, number);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка добавления элемента!");
        }
    }

    public static void setSecondPartArray(int number, int pos) {
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set secondpart[" + pos + "]=? where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, number);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка добавления элемента!");
        }
    }

    public static void setResult(int number) {
        int pos = getResultLength();
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set result[" + (pos+1) + "]=? where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, number);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка добавления элемента!");
        }
    }
    public static void clearResult()
    {
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set result=null where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка очистки массива!");
        }
    }
    public static void clearFirstPart()
    {
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set firstpart=null where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка очистки массива!");
        }
    }
    public static void clearSecondPart()
    {
        try {
            setCopyOfIteration();
            Connection connection = connectToDB();
            String sql = "update lab5iterations set secondpart=null where id="+id+" and countOfIteration="+countOfIteration;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка очистки массива!");
        }
    }
    public static ArrayList<Integer> getResult()
    {
        ArrayList<Integer> arrayList=new ArrayList<>();
        for(int i=0;i<getResultLength();i++)
        {
            arrayList.add(getResultElem(i+1));
        }
        return arrayList;
    }
    private static void setCopyOfIteration()
    {
        try {
            Connection connection = connectToDB();
            String sql = "insert into lab5iterations (secondpart, firstpart, id, result) select secondpart, firstpart, id, result from lab5iterations where id="+id+" order by countofiteration desc limit 1";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            String sql2="update lab5iterations set countofiteration="+(countOfIteration+1)+" where id="+id+" and countofiteration=-1";
            PreparedStatement stmt2 = connection.prepareStatement(sql2);
            stmt2.executeUpdate();
            countOfIteration++;
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка очистки таблицы!");
        }
    }
    public static void setCurrentId()
    {
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select max(id) from lab5");
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                id = rs.getInt(1);
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения длины массива!");
        }
    }
    public static int getCountOfIteration()
    {
        return countOfIteration;
    }
    public static void setSortedResult()
    {
        try {
            Connection connection = connectToDB();
            String sql = "update lab5 set result=(select result from lab5iterations where id="+id+" and countofiteration="+countOfIteration+") where id="+id;
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка очистки таблицы!");
        }
    }
    public static int getId()
    {
        return id;
    }
    public static String getStartArray()
    {
        String str="";
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select startarray from lab5 where id="+id);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                str = rs.getArray(1).toString();
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения массива!");
        }
        return str;
    }
    public static String getResultToWrite()
    {
        String str="";
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select result from lab5 where id="+id);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                str = rs.getArray(1).toString();
            } while (rs.next());
            rs.close();
            stmt.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения массива!");
        }
        return str;
    }
    public static String getCurrentIteration(int iteration)
    {
        String str=iteration+") ";
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select firstpart from lab5iterations where id="+id+" and countofiteration="+iteration);
            if (rs.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                if(rs.getArray(1)!=null)
                    str = str + rs.getArray(1).toString()+" ";
            } while (rs.next());
            rs.close();
            stmt.close();
            Statement stmt2 = connection.createStatement();
            ResultSet rs2 = stmt2.executeQuery("select secondpart from lab5iterations where id="+id+" and countofiteration="+iteration);
            if (rs2.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                if(rs2.getArray(1)!=null)
                    str = str + rs2.getArray(1).toString()+" ";
            } while (rs2.next());
            rs2.close();
            stmt2.close();
            Statement stmt3 = connection.createStatement();
            ResultSet rs3 = stmt3.executeQuery("select result from lab5iterations where id="+id+" and countofiteration="+iteration);
            if (rs3.next() == false) {
                System.out.println("Ошибка забора данных!");
            }
            do {
                if(rs3.getArray(1)!=null)
                    str = str + rs3.getArray(1).toString();
            } while (rs3.next());
            str=str+"\r\n";
            rs3.close();
            stmt3.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка получения массива!");
        }
        return str;
    }
    public static void setGraph(String str)
    {
        try {
            Connection connection = connectToDB();
            String sql = "insert into lab6aasd values (?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, str);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка ввода нового графа!");
        }
    }
    public static void setValueLab7(String str){
        try {
            Connection connection = connectToDB();
            String sql = "insert into lab7 values (?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, str);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка ввода нового графа!");
        }
    }
    public static void setValueLab8(String str){
        try {
            Connection connection = connectToDB();
            String sql = "insert into lab8 values (?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, str);
            stmt.executeUpdate();
            connection.close();
        } catch (Exception e) {
            System.out.println("Ошибка ввода нового графа!");
        }
    }
}
