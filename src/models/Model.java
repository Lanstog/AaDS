package models;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import views.Lab5Window;
import views.Lab6Window;
import views.Lab7Window;
import views.Lab8Window;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;

public abstract class Model {
    private static int quickCounter;
    private static ArrayList<ArrayList<Integer>> connections;
    private static ArrayList<Integer> alreadyVisited;
    private static class GraphData {
        int mark;
        int way;
        boolean visited;
    }
    public static void sortBubble(String str, Object window) {
        ArrayList<Integer> arrayList=convertString(str);
        if(arrayList==null)
            return;
        int counter = 0;
        System.out.print("Изначальный массив: ");
        for (int i = 0; i < arrayList.size(); i++) {
            if (i == arrayList.size() - 1)
                System.out.println(arrayList.get(i) + " ");
            else
                System.out.print(arrayList.get(i) + " ");
        }
        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 0; j < arrayList.size() - 1; j++) {
                if (arrayList.get(j) > arrayList.get(j + 1)) { //Поменять элементы местами
                    int swap = arrayList.get(j);
                    arrayList.set(j, arrayList.get(j + 1));
                    arrayList.set(j + 1, swap);
                    counter++;
                    System.out.print("Результат итерации: ");
                    for (int k = 0; k < arrayList.size(); k++) {
                        if (k == j)
                            System.out.print("*" + arrayList.get(k) + "*" + " ");
                        else {
                            if (k == arrayList.size() - 1)
                                System.out.println(arrayList.get(k) + " ");
                            else
                                System.out.print(arrayList.get(k) + " ");
                        }
                    }
                }
            }
        }
        outputInfo(arrayList, counter, window);
    }

    public static void sortThrough(String str, Object window) {
        ArrayList<Integer> arrayList=convertString(str);
        if(arrayList==null)
            return;
        int counter = 0;
        System.out.print("Изначальный массив: ");
        for (int i = 0; i < arrayList.size(); i++) {
            if (i == arrayList.size() - 1)
                System.out.println(arrayList.get(i) + " ");
            else
                System.out.print(arrayList.get(i) + " ");
        }
        for (int i = 0; i < arrayList.size(); i++) {
            int min = arrayList.get(i);
            for (int j = 0; i + j < arrayList.size(); j++) {
                if (min > arrayList.get(j + i)) { //Переставить элемент в конец отсортированной части массива
                    min = arrayList.get(j + i);
                    arrayList.remove(j + i);
                    arrayList.add(i, min);
                    counter++;
                    System.out.print("Результат итерации: ");
                    for (int k = 0; k < arrayList.size(); k++) {
                        if (i == k)
                            System.out.print("*" + arrayList.get(k) + "*" + " ");
                        else {
                            if (k == arrayList.size() - 1)
                                System.out.println(arrayList.get(k) + " ");
                            else
                                System.out.print(arrayList.get(k) + " ");
                        }
                    }
                }
            }
        }
        outputInfo(arrayList, counter, window);
    }

    public static void shakeSort(String str, Object window)
    {
        ArrayList<Integer> arrayList=convertString(str);
        if(arrayList==null)
            return;
        int counter = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            boolean swapEl=false;
            if(i%2==0) {
                for (int j = 0; j < arrayList.size() - 1; j++) {
                    if (arrayList.get(j) > arrayList.get(j + 1)) { //Поменять элементы местами
                        int swap = arrayList.get(j);
                        arrayList.set(j, arrayList.get(j + 1));
                        arrayList.set(j + 1, swap);
                        counter++;
                        swapEl=true;
                    }
                }
            }
            else
            {
                for (int j = arrayList.size()-1; j > 0; j--) {
                    if (arrayList.get(j) < arrayList.get(j - 1)) { //Поменять элементы местами
                        int swap = arrayList.get(j);
                        arrayList.set(j, arrayList.get(j - 1));
                        arrayList.set(j - 1, swap);
                        counter++;
                        swapEl=true;
                    }
                }
            }
            if(swapEl==false)
                break;
        }
        outputInfo(arrayList, counter, window);
    }

    public static void quickSort(String str, Object window)
    {
        ArrayList<Integer> arrayList=convertString(str);
        if(arrayList==null)
            return;
        quickCounter=0;
        swapElementsQuick(arrayList, 0, arrayList.size()-1);
        outputInfo(arrayList, quickCounter, window);
    }
    public static void swapElementsQuick(ArrayList<Integer> arrayList, int start, int end)
    {
        if(start>=end)
            return;
        int index=start+(end-start)/2;
        int p=arrayList.get(index);
        int i=start, j=end;
        while (i<=j)
        {
            while (arrayList.get(i)<p)
            {
                i++;
            }
            while (arrayList.get(j)>p)
            {
                j--;
            }
            if(i<=j)
            {
                int swap=arrayList.get(i);
                arrayList.set(i, arrayList.get(j));
                arrayList.set(j, swap);
                i++;
                j--;
                quickCounter++;
            }
        }
        if(start<j)
            swapElementsQuick(arrayList, start, j);
        if(end>i)
            swapElementsQuick(arrayList, i, end);
    }

    private static ArrayList<Integer> convertString(String str) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        String[] strList = str.split("\\s"); //Разбить строку на массив строк
        try {
            for (int i = 0; i < strList.length; i++) {
                arrayList.add(Integer.parseInt(strList[i])); //Перевод конкретной строки в число
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ошибка конвертирования! (лишние символы в строке)");
            return null;
        }
        if(arrayList.size()==0)
        {
            return null;
        }
        return arrayList;
    }

    private static void outputInfo(ArrayList<Integer> arrayList, int counter, Object window) { //Отослать сообщение для вывода
        String strOut = "Количество перемещений: " + counter + "\nОтсортированный массив: ";
        for (int i = 0; i < arrayList.size(); i++) {
            strOut = strOut + arrayList.get(i) + " ";
        }
        try
        {
            if(window instanceof Lab5Window)
            {
                strOut=strOut+"\n";
                for(int i=1;i<=DataBaseModel.getCountOfIteration();i++)
                {
                    strOut=strOut+DataBaseModel.getCurrentIteration(i);
                }
            }
            Method method=window.getClass().getMethod("setResultText", String.class);
            method.invoke(window, strOut);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Метод вывода не был получен!");
            return;
        }
    }

    public static void simpleMerge(String str, Object window)
    {
        convertStrToArrayAndSend(str);
        DataBaseModel.setCurrentId();
        int length=DataBaseModel.getResultLength();
        if(length<0)
        {
            JOptionPane.showMessageDialog(null, "Длина массива не была получена!");
            return;
        }
        for(int i=1; i<length;i=i*2)
        {
            boolean mas=false;
            for(int j=0;j<length;)
            {
                for(int k=0;k<i;k++)
                {
                    if(j>=length)
                        break;
                    int number=DataBaseModel.getResultElem(j+1);
                    if(mas==false) {
                        DataBaseModel.setFirstPartArray(number, DataBaseModel.getFirstPartLength() + 1);
                        j++;
                    }
                    else {
                        DataBaseModel.setSecondPartArray(number, DataBaseModel.getSecondPartLength() + 1);
                        j++;
                    }
                    if(k==i-1)
                        mas=mas==false?true:false;
                }
            }
            DataBaseModel.clearResult();
            int counter=0;
            int counterOfBlocks=0;
            int firstPartLength=0;
            int secondPartLength=0;
            while(counter<length)
            {
                int a=0;
                int b=0;
                while(a<i&&b<i&&firstPartLength<DataBaseModel.getFirstPartLength()&&secondPartLength<DataBaseModel.getSecondPartLength())
                {
                    int aElem=DataBaseModel.getFirstPartElem(counterOfBlocks*i+a+1);
                    int bElem=DataBaseModel.getSecondPartElem(counterOfBlocks*i+b+1);
                    if(aElem>bElem)
                    {
                        DataBaseModel.setResult(bElem);
                        secondPartLength++;
                        b++;
                    }
                    else
                    {
                        DataBaseModel.setResult(aElem);
                        firstPartLength++;
                        a++;
                    }
                    counter++;
                }
                while(a<i&&firstPartLength<DataBaseModel.getFirstPartLength())
                {
                    int aElem=DataBaseModel.getFirstPartElem(counterOfBlocks*i+a+1);
                    DataBaseModel.setResult(aElem);
                    a++;
                    counter++;
                    firstPartLength++;
                }
                while(b<i&&secondPartLength<DataBaseModel.getSecondPartLength())
                {
                    int bElem=DataBaseModel.getSecondPartElem(counterOfBlocks*i+b+1);
                    DataBaseModel.setResult(bElem);
                    b++;
                    counter++;
                    secondPartLength++;
                }
                counterOfBlocks++;
            }
            DataBaseModel.clearFirstPart();
            DataBaseModel.clearSecondPart();
        }
        DataBaseModel.setSortedResult();
        ArrayList<Integer> arrayList=DataBaseModel.getResult();
        outputInfo(arrayList, DataBaseModel.getCountOfIteration(), window);
        writeToNewFile();
    }
    private static void convertStrToArrayAndSend(String str)
    {
        ArrayList<Integer> arrayList=convertString(str);
        DataBaseModel.sendArray(arrayList);
    }
    private static void writeToNewFile()
    {
        try {
            String length = String.valueOf(DataBaseModel.getId());
            FileWriter fileWriter = new FileWriter("./Lab №5 results/" + length + ".txt");
            fileWriter.write("Изначальный массив: "+DataBaseModel.getStartArray()+"\r\n");
            fileWriter.write("Отсортированный массив: "+DataBaseModel.getResultToWrite()+"\r\n");
            for(int i=1;i<=DataBaseModel.getCountOfIteration();i++)
            {
                fileWriter.write(DataBaseModel.getCurrentIteration(i));
            }
            fileWriter.close();
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Не удалось записать результаты в файл!");
        }
    }
    public static void circumvention(Object window, mxGraph mxgraph, Object[] verteces)
    {
        connections=new ArrayList<>();
        for(int i=0;i<verteces.length;i++)
        {
            ArrayList<Integer> connect=new ArrayList<>();
            for(int j=0;j<verteces.length;j++)
            {
                if(mxgraph.getEdgesBetween(verteces[i], verteces[j], true)!=null) {
                    if (mxgraph.getEdgesBetween(verteces[i], verteces[j], true).length != 0) {
                        connect.add(j);
                    }
                }
            }
            connections.add(connect);
        }
        alreadyVisited=new ArrayList<>();
        alreadyVisited.add(0);
        String result="1";
        result=result+circumFor(mxgraph, verteces, 0);
        try
        {
            Method method=window.getClass().getMethod("setValue", String.class);
            method.invoke(window, result);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Метод вывода не был получен!");
            return;
        }
    }
    private static String circumFor(mxGraph mxgraph, Object[] verteces, int currentVertex)
    {
        String result="";
        for(int i=0;i<verteces.length;i++) {
            if (mxgraph.getEdgesBetween(verteces[currentVertex], verteces[i], true).length != 0 && alreadyExist(i, alreadyVisited) == false) {
                alreadyVisited.add(i);
                result=result+", " + (i + 1)+circumFor(mxgraph, verteces, i);
            }
        }
        return result;
    }
    private static boolean alreadyExist(int currentVertex, ArrayList<Integer> alreadyVisited)
    {
        for(int i=0;i<alreadyVisited.size();i++)
        {
            if(alreadyVisited.get(i)==currentVertex)
            {
                return true;
            }
        }
        return false;
    }
    public static void findCloseWay(Object window, mxGraph mxgraph, Object[] verteces, int index) {
        if (index > verteces.length) {
            JOptionPane.showMessageDialog(null, "Данной вершины не существует!");
            return;
        }
        for (int i = 0; i < verteces.length; i++) {
            for (int j = 0; j < verteces.length; j++) {
                if (mxgraph.getEdgesBetween(verteces[i], verteces[j], true).length != 0) {
                    Object[] edge = mxgraph.getEdgesBetween(verteces[i], verteces[j], true);
                    if (mxgraph.getModel().getValue(edge[0]).equals("")) {
                        JOptionPane.showMessageDialog(null, "Одно из рёбер не имеет веса!");
                        return;
                    }
                }
            }
        }
        boolean stop = true;
        for (int i = 0; i < verteces.length; i++) {
            if (mxgraph.getEdgesBetween(verteces[index - 1], verteces[i], true).length != 0) {
                stop = false;
                break;
            }
        }
        if (stop == true) {
            JOptionPane.showMessageDialog(null, "Из данной вершины нет исходящих рёбер!");
            return;
        }
        ArrayList<GraphData> graphDataArrayList = new ArrayList<>();
        for (int i = 0; i < verteces.length; i++) {
            GraphData graphData = new GraphData();
            graphData.way = 0;
            if (i + 1 == index) {
                graphData.mark = 0;
                graphData.visited = true;
            } else {
                graphData.mark = Integer.MAX_VALUE;
                graphData.visited = false;
            }
            graphDataArrayList.add(graphData);
        }
        int w = index - 1;
        int counter = 0;
        while (counter < verteces.length) {
            for (int i = 0; i < verteces.length; i++) {
                if (mxgraph.getEdgesBetween(verteces[w], verteces[i], true).length != 0) {
                    Object[] edge = mxgraph.getEdgesBetween(verteces[w], verteces[i], true);
                    if (graphDataArrayList.get(i).mark > Integer.parseInt(mxgraph.getModel().getValue(edge[0]).toString()) + graphDataArrayList.get(w).mark) {
                        graphDataArrayList.get(i).mark = Integer.parseInt(mxgraph.getModel().getValue(edge[0]).toString()) + graphDataArrayList.get(w).mark;
                        graphDataArrayList.get(i).way = w + 1;
                    }
                }
            }
            ArrayList<GraphData> graphData = new ArrayList<>();
            for (int i = 0; i < graphDataArrayList.size(); i++) {
                if (graphDataArrayList.get(i).visited == false) {
                    graphData.add(graphDataArrayList.get(i));
                }
            }
            if (graphData.size() == 0)
                break;
            graphData = null;
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < graphDataArrayList.size(); i++) {
                if (min > graphDataArrayList.get(i).mark && graphDataArrayList.get(i).visited == false) {
                    w = i;
                    min = graphDataArrayList.get(i).mark;
                }
            }
            graphDataArrayList.get(w).visited = true;
            counter++;
        }
        String out="Исходная вершина: "+index+"\n";
        for(int i=0;i<graphDataArrayList.size();i++){
            if(i+1!=index){
                out=out+"До вершины "+(i+1)+": ";
                if(graphDataArrayList.get(i).visited==false){
                    out=out+"В данную вершину невозможно попасть\n";
                }
                else{
                    out=out+"длина пути: "+graphDataArrayList.get(i).mark+", кратчайший путь: ";
                    String outWay="";
                    int curWay=graphDataArrayList.get(i).way;
                    while(curWay!=index){
                        outWay="-"+curWay+outWay;
                        curWay=graphDataArrayList.get(curWay-1).way;
                    }
                    out=out+curWay+outWay+"\n";
                }
            }
        }
        try
        {
            Method method=window.getClass().getMethod("setValue", String.class);
            method.invoke(window, out);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Метод вывода не был получен!");
            return;
        }
    }

    public static void findTree(Lab8Window lab8Window, Object[] verteces, mxGraph mxgraph, mxGraphComponent mxgraphComponent, JScrollPane jScrollPane){
        //int startVetrex=Integer.MAX_VALUE;
        String connections="";
        mxgraph.getModel().beginUpdate();
        try {
            setBackColors(mxgraph, verteces);
        ArrayList<Object> copyVerteces=new ArrayList<>();
        copyVerteces.add(verteces[0]);
        for(int i=1;i<verteces.length;i++){
            int min=Integer.MAX_VALUE;
            int indexInMas=0;
            int index=0;
            boolean change=false;
            for(int j=0;j<copyVerteces.size();j++){
                for(int k=0;k<verteces.length;k++){
                    if(!existAlready(copyVerteces, verteces[k])){
                        if(mxgraph.getEdgesBetween(copyVerteces.get(j), verteces[k]).length!=0) {
                            if(min>Integer.parseInt(mxgraph.getModel().getValue(mxgraph.getEdgesBetween(copyVerteces.get(j), verteces[k])[0]).toString())){
                                min=Integer.parseInt(mxgraph.getModel().getValue(mxgraph.getEdgesBetween(copyVerteces.get(j), verteces[k])[0]).toString());
                                indexInMas=j;
                                index=k;
                                change=true;
                            }
                        }
                    }
                }
            }
            if(change) {
                mxgraph.setCellStyles(mxConstants.STYLE_STROKECOLOR, "red", mxgraph.getEdgesBetween(verteces[index], copyVerteces.get(indexInMas)));
                copyVerteces.add(verteces[index]);
                if(connections.equals("")){
                    connections=connections+mxgraph.getModel().getValue(verteces[index])+"-"+mxgraph.getModel().getValue(copyVerteces.get(indexInMas));
                }
                else{
                    connections=connections+", "+mxgraph.getModel().getValue(verteces[index])+"-"+mxgraph.getModel().getValue(copyVerteces.get(indexInMas));
                }
            }
        }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        DataBaseModel.setValueLab8(connections);
    }

    private static void setBackColors(mxGraph mxgraph, Object[] verteces){
        mxgraph.setCellStyles(mxConstants.STYLE_STROKECOLOR,"blue", mxgraph.getAllEdges(verteces));
    }

    private static boolean existAlready(ArrayList<Object>verteces, Object vertex){
        for(int i=0;i<verteces.size();i++){
            if(verteces.get(i)==vertex){
                return true;
            }
        }
        return false;
    }
}
