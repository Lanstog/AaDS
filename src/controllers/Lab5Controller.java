package controllers;

import models.Model;
import views.Lab5Window;

public abstract class Lab5Controller {
    public static void getObject()
    {
        Lab5Window.getObject().setVisible(true);
    }
    public static void initiateWork(String str, Lab5Window lab5Window)
    {
        Model.simpleMerge(str, lab5Window);
    }
}
