package controllers;
import models.Model;
import views.Lab3Window;

import java.util.ArrayList;

public abstract class Lab3Controller {
    public static void initiateWork(int func, String str, Object window) { //Подготовка к работе
        if (func == 0) {
            Model.sortThrough(str, window); //Сортировка вставками
        } else {
            Model.sortBubble(str, window); //Пузырьковая сортировка
        }
    }
    public static void getObject()
    {
        Lab3Window.getObject().setVisible(true);
    }
}
