package controllers;

import models.Model;
import views.Lab4Window;

public abstract class Lab4Controller {
    public static void initiateWork(int func, String str, Lab4Window lab4Window)
    {
        if(func==0)
        {
            Model.shakeSort(str, lab4Window);
        }
        else
            Model.quickSort(str, lab4Window);
    }
    public static void getObject()
    {
        Lab4Window.getObject().setVisible(true);
    }
}
