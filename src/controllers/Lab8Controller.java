package controllers;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import models.Model;
import views.Lab7Window;
import views.Lab8Window;

import javax.swing.*;

public abstract class Lab8Controller {
    public static void getObject()
    {
        Lab8Window.getObject().setVisible(true);
    }
    public static void findTree(Lab8Window lab8Window, mxGraph mxgraph, Object[] verteces, mxGraphComponent mxgraphComponent, JScrollPane jScrollPane) {
        Model.findTree(lab8Window, verteces, mxgraph, mxgraphComponent, jScrollPane);
        //lab8Window.updateView();
    }
}
