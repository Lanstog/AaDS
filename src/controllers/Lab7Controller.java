package controllers;

import com.mxgraph.view.mxGraph;
import models.Model;
import views.Lab7Window;

import javax.swing.*;

public abstract class Lab7Controller {
    public static void getObject()
    {
        Lab7Window.getObject().setVisible(true);
    }
    public static void findCloseWay(Lab7Window lab7Window, mxGraph mxgraph, Object[] verteces, int index)
    {
        Model.findCloseWay(lab7Window, mxgraph, verteces, index);
    }
}
