package controllers;

import com.mxgraph.view.mxGraph;
import models.Model;
import views.Lab6Window;

public abstract class Lab6Controller {
    public static void getObject()
    {
        Lab6Window.getObject().setVisible(true);
    }
    public static void circumvention(Lab6Window lab6Window, mxGraph mxgraph, Object[] verteces)
    {
        Model.circumvention(lab6Window, mxgraph, verteces);
    }
}
