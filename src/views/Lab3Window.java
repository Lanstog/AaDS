package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controllers.Lab3Controller;

public class Lab3Window extends JDialog {
    private JPanel contentPane;
    private JTextField arrayField;
    private JButton okButton;
    private JTextArea resultField;
    private JRadioButton bubbleSort;
    private JRadioButton throughSort;
    private static Lab3Window lab3Window;

    private Lab3Window(MainWindow mainWindow) {
        super(mainWindow, "Сортировка", true);
        setContentPane(contentPane);
        setMinimumSize(new Dimension(450, 250));
        getRootPane().setDefaultButton(okButton);
        setLocationRelativeTo(null);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (arrayField.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Строка пуста!");
                    return;
                }
                int func = 0; //Сортировка вставками
                if (bubbleSort.isSelected()) //Пузырьковая сортировка
                    func = 1;
                Lab3Controller.initiateWork(func, arrayField.getText(), lab3Window);
            }
        });
    }

    public void setResultText(String strOut) {
        resultField.setText(strOut);
    }

    public static Lab3Window getObject() {
        if (lab3Window == null)
            lab3Window = new Lab3Window(MainWindow.getObject());
        return lab3Window;
    }
}
