package views;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SupWindowForLab6 extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField numField;
    private JTextField firstField;
    private JTextField secondField;
    private JLabel numLab;
    private JLabel firstVerLab;
    private JLabel secondVerLab;
    private JTextField edgeValue;
    private JLabel edgeValueLabel;

    public SupWindowForLab6(Lab6Window lab6Window, int mode) {
        super(lab6Window);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        if(mode==0)
        {
            numField.setVisible(true);
            numLab.setVisible(true);
            firstField.setVisible(false);
            secondField.setVisible(false);
            firstVerLab.setVisible(false);
            secondVerLab.setVisible(false);
        }
        else
        {
            numField.setVisible(false);
            numLab.setVisible(false);
            firstField.setVisible(true);
            secondField.setVisible(true);
            firstVerLab.setVisible(true);
            secondVerLab.setVisible(true);
        }
        pack();
        setLocationRelativeTo(null);
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(mode==0)
                {
                    if(numField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab6Window.removeVertex(Integer.parseInt(numField.getText()));
                    setVisible(false);
                }
                else
                {
                    if(firstField.getText().isEmpty()||secondField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab6Window.removeEdge(Integer.parseInt(firstField.getText()), Integer.parseInt(secondField.getText()));
                    setVisible(false);
                }
            }
        });
        this.setVisible(true);
    }

    public SupWindowForLab6(Lab8Window lab8Window, int mode) {
        super(lab8Window);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        if(mode==0)
        {
            numField.setVisible(true);
            numLab.setVisible(true);
            firstField.setVisible(false);
            secondField.setVisible(false);
            firstVerLab.setVisible(false);
            secondVerLab.setVisible(false);
            edgeValue.setVisible(false);
            edgeValueLabel.setVisible(false);
        }
        if(mode==1)
        {
            numField.setVisible(false);
            numLab.setVisible(false);
            firstField.setVisible(true);
            secondField.setVisible(true);
            firstVerLab.setVisible(true);
            secondVerLab.setVisible(true);
            edgeValue.setVisible(false);
            edgeValueLabel.setVisible(false);
        }
        if(mode==2){
            numField.setVisible(false);
            numLab.setVisible(false);
            firstField.setVisible(true);
            secondField.setVisible(true);
            firstVerLab.setVisible(true);
            secondVerLab.setVisible(true);
            edgeValue.setVisible(true);
            edgeValueLabel.setVisible(true);
        }
        pack();
        setLocationRelativeTo(null);
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(mode==0)
                {
                    if(numField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab8Window.removeVertex(Integer.parseInt(numField.getText()));
                    setVisible(false);
                }
                if(mode==1)
                {
                    if(firstField.getText().isEmpty()||secondField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab8Window.removeEdge(Integer.parseInt(firstField.getText()), Integer.parseInt(secondField.getText()));
                    setVisible(false);
                }
                if(mode==2){
                    if(firstField.getText().isEmpty()||secondField.getText().isEmpty()||edgeValue.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab8Window.setEdge(Integer.parseInt(firstField.getText()), Integer.parseInt(secondField.getText()), Integer.parseInt(edgeValue.getText()));
                    setVisible(false);
                }
            }
        });
        this.setVisible(true);
    }
    public SupWindowForLab6(Lab7Window lab7Window, int mode) {
        super(lab7Window);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        if(mode==0)
        {
            numField.setVisible(true);
            numLab.setVisible(true);
            firstField.setVisible(false);
            secondField.setVisible(false);
            firstVerLab.setVisible(false);
            secondVerLab.setVisible(false);
        }
        else
        {
            numField.setVisible(false);
            numLab.setVisible(false);
            firstField.setVisible(true);
            secondField.setVisible(true);
            firstVerLab.setVisible(true);
            secondVerLab.setVisible(true);
        }
        pack();
        setLocationRelativeTo(null);
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(mode==0)
                {
                    if(numField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab7Window.removeVertex(Integer.parseInt(numField.getText()));
                    setVisible(false);
                }
                else
                {
                    if(firstField.getText().isEmpty()||secondField.getText().isEmpty())
                    {
                        JOptionPane.showMessageDialog(null, "Введите значение!");
                        return;
                    }
                    lab7Window.removeEdge(Integer.parseInt(firstField.getText()), Integer.parseInt(secondField.getText()));
                    setVisible(false);
                }
            }
        });
        this.setVisible(true);
    }
}
