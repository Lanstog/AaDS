package views;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import controllers.Lab7Controller;
import models.DataBaseModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Lab7Window extends JDialog {
    private JPanel contentPane;
    private JButton findWayButton;
    private JButton addVertex;
    private JButton deleteVertexButton;
    private JButton deleteEdgeButton;
    private JScrollPane jScrollPane;
    private JLabel circumResult;
    private JTextArea resultArea;
    private JTextField inputValueField;
    private mxGraph mxgraph;
    private mxGraphComponent mxgraphComponent;
    private static Lab7Window lab7Window;
    private Object parent;
    private int countForPosition;
    private Object[] verteces;

    private Lab7Window() {
        setContentPane(contentPane);
        setModal(true);
        setTitle("Лабораторная №7");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        countForPosition=0;
        mxgraph = new mxGraph();
        parent=mxgraph.getDefaultParent();
        mxgraphComponent = new mxGraphComponent(mxgraph);
        verteces=new Object[0];
        jScrollPane.getViewport().add(mxgraphComponent, null);
        pack();
        setLocationRelativeTo(null);
        addVertex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVertex();
            }
        });
        deleteVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab7Window, 0);
            }
        });
        deleteEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab7Window, 1);
            }
        });
        findWayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //try {
                    if(inputValueField.getText().isEmpty()){
                        JOptionPane.showMessageDialog(null, "Введите число!");
                        return;
                    }
                    Lab7Controller.findCloseWay(lab7Window, mxgraph, verteces, Integer.parseInt(inputValueField.getText()));
                /*}
                catch (Exception ex){
                    JOptionPane.showMessageDialog(null, "Введено не число!");
                }*/
            }
        });
    }
    public static Lab7Window getObject()
    {
        if(lab7Window==null)
        {
            lab7Window=new Lab7Window();
        }
        return lab7Window;
    }
    public void addVertex()
    {
        mxgraph.getModel().beginUpdate();
        try
        {
            Object v1 = mxgraph.insertVertex(parent, null, verteces.length+1, 20+countForPosition*80, 20, 50, 50);
            countForPosition++;
            Object[] newVerteces=new Object[verteces.length+1];
            for(int i=0;i<verteces.length;i++)
            {
                newVerteces[i]=verteces[i];
            }
            newVerteces[verteces.length]=v1;
            verteces=newVerteces;
        }
        finally
        {
            // Updates the display
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
    }
    public void setValue(String str)
    {
        resultArea.setText(str);
        DataBaseModel.setValueLab7(str);
    }
    public void removeVertex(int value)
    {
        if(verteces.length==0)
        {
            JOptionPane.showMessageDialog(null, "Вершин нет!");
            return;
        }
        if(value>verteces.length)
        {
            JOptionPane.showMessageDialog(null, "Данной вершины не существует!");
            return;
        }
        Object[] newVerteces=new Object[verteces.length-1];
        int add=0;
        for(int i=0;i<verteces.length;i++)
        {
            if(i!=(value-1)) {
                newVerteces[i-add] = verteces[i];
            }
            else
            {
                add++;
            }
        }
        mxgraph.getModel().beginUpdate();
        try {
            mxgraph.removeCells(new Object[]{verteces[value-1]});
            for(int i=0;i<newVerteces.length;i++)
            {
                mxgraph.getModel().setValue(newVerteces[i], i+1);
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        verteces=newVerteces;
    }
    public void removeEdge(int firstVertex, int secondVertex)
    {
        try {
            if(mxgraph.getEdgesBetween(verteces[firstVertex-1], verteces[secondVertex-1], true).length!=0) {
                mxgraph.getModel().remove(mxgraph.getEdgesBetween(verteces[firstVertex - 1], verteces[secondVertex - 1], true)[0]);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Нет такой связи!");
                return;
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
    }
}
