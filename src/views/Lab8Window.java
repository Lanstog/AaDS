package views;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import controllers.Lab8Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Lab8Window extends JDialog {
    private JPanel contentPane;
    private JScrollPane jScrollPane;
    private JButton addVertex;
    private JButton deleteVertexButton;
    private JButton deleteEdgeButton;
    private JButton addEdgeButton;
    private JButton buttonOK;
    private mxGraph mxgraph;
    private mxGraphComponent mxgraphComponent;
    private static Lab8Window lab8Window;
    private Object parent;
    private int countForPosition;
    private Object[] verteces;

    public Lab8Window() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);setContentPane(contentPane);
        setModal(true);
        setTitle("Лабораторная №8");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        countForPosition=0;
        mxgraph = new mxGraph();
        parent=mxgraph.getDefaultParent();
        mxgraphComponent = new mxGraphComponent(mxgraph);
        //mxgraphComponent.setEnabled(false);
        verteces=new Object[0];
        jScrollPane.getViewport().add(mxgraphComponent, null);
        pack();
        setLocationRelativeTo(null);
        addVertex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVertex();
            }
        });
        deleteVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab8Window, 0);
            }
        });
        deleteEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab8Window, 1);
            }
        });
        addEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab8Window, 2);
            }
        });
    }

    public static Lab8Window getObject()
    {
        if(lab8Window==null)
        {
            lab8Window=new Lab8Window();
        }
        return lab8Window;
    }
    public void addVertex()
    {
        mxgraph.getModel().beginUpdate();
        try
        {
            Object v1=null;
            if(verteces.length>4)
            {
                v1 = mxgraph.insertVertex(parent, null, verteces.length+1, 20+(countForPosition-5)*80, 120, 50, 50);
            }
            else
                v1 = mxgraph.insertVertex(parent, null, verteces.length+1, 20+countForPosition*80, 20, 50, 50);
            countForPosition++;
            Object[] newVerteces=new Object[verteces.length+1];
            for(int i=0;i<verteces.length;i++)
            {
                newVerteces[i]=verteces[i];
            }
            newVerteces[verteces.length]=v1;
            verteces=newVerteces;
        }
        finally
        {
            // Updates the display
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
    }
    public void removeVertex(int value)
    {
        if(verteces.length==0)
        {
            JOptionPane.showMessageDialog(null, "Вершин нет!");
            return;
        }
        if(value>verteces.length)
        {
            JOptionPane.showMessageDialog(null, "Данной вершины не существует!");
            return;
        }
        Object[] newVerteces=new Object[verteces.length-1];
        int add=0;
        for(int i=0;i<verteces.length;i++)
        {
            if(i!=(value-1)) {
                newVerteces[i-add] = verteces[i];
            }
            else
            {
                add++;
            }
        }
        mxgraph.getModel().beginUpdate();
        try {
            mxgraph.removeCells(new Object[]{verteces[value-1]});
            for(int i=0;i<newVerteces.length;i++)
            {
                mxgraph.getModel().setValue(newVerteces[i], i+1);
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        verteces=newVerteces;
        Lab8Controller.findTree(lab8Window, mxgraph, verteces, mxgraphComponent, jScrollPane);
    }
    public void removeEdge(int firstVertex, int secondVertex)
    {
        mxgraph.getModel().beginUpdate();
        try {
            if(mxgraph.getEdgesBetween(verteces[firstVertex-1], verteces[secondVertex-1], true).length!=0) {
                mxgraph.getModel().remove(mxgraph.getEdgesBetween(verteces[firstVertex - 1], verteces[secondVertex - 1], true)[0]);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Нет такой связи!");
                return;
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        Lab8Controller.findTree(lab8Window, mxgraph, verteces, mxgraphComponent, jScrollPane);
    }
    public void setEdge(int firstVertex, int secondVertex, int value) {
        mxgraph.getModel().beginUpdate();
        try {
            if(!(firstVertex>verteces.length||secondVertex>verteces.length)) {
                Object e1 = mxgraph.insertEdge(parent, null, value, verteces[firstVertex - 1], verteces[secondVertex - 1]);
                Object e2 = mxgraph.insertEdge(parent, null, value, verteces[secondVertex - 1], verteces[firstVertex - 1]);
            }
            else {
                JOptionPane.showMessageDialog(null, "Одной из вершин не существует!");
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        Lab8Controller.findTree(lab8Window, mxgraph, verteces, mxgraphComponent, jScrollPane);
    }
    public void updateView(){
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
    }
}
