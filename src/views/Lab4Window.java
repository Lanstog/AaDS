package views;

import controllers.Lab4Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Lab4Window extends JDialog {
    private JPanel contentPane;
    private JTextField arrayField;
    private JTextArea resultField;
    private JRadioButton shakeSort;
    private JRadioButton quickSort;
    private JButton okButton;
    private static Lab4Window lab4Window;

    private Lab4Window(MainWindow mainWindow) {
        super(mainWindow,"Лабораторная №4", true);
        setContentPane(contentPane);
        pack();
        setMinimumSize(new Dimension(450, 250));
        getRootPane().setDefaultButton(okButton);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (arrayField.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Строка пуста!");
                    return;
                }
                int func = 0; //Шейкерная сортировка
                if (quickSort.isSelected()) //Быстрая сортировка
                    func = 1;
                Lab4Controller.initiateWork(func, arrayField.getText(), lab4Window);
            }
        });
    }
    public static Lab4Window getObject() {
        if (lab4Window == null)
            lab4Window = new Lab4Window(MainWindow.getObject());
        return lab4Window;
    }
    public void setResultText(String strOut) {
        resultField.setText(strOut);
    }
}
