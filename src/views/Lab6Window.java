package views;

import javax.swing.*;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import controllers.Lab6Controller;
import models.DataBaseModel;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Lab6Window extends JDialog {
    private JPanel contentPane;
    private JButton circumButton;
    private JScrollPane jScrollPane;
    private JLabel circumResult;
    private JButton addVertex;
    private JButton deleteVertexButton;
    private JButton deleteEdgeButton;
    private JButton backToIndexesButton;
    private JPanel graphPanel;
    private mxGraph mxgraph;
    private mxGraphComponent mxgraphComponent;
    private static Lab6Window lab6Window;
    private Object parent;
    private int countForPosition;
    private Object[] verteces;

    private Lab6Window() {
        setContentPane(contentPane);
        setModal(true);
        setTitle("Лабораторная №6");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        countForPosition=0;
        getRootPane().setDefaultButton(circumButton);
        mxgraph = new mxGraph();
        parent=mxgraph.getDefaultParent();
        mxgraphComponent = new mxGraphComponent(mxgraph);
        verteces=new Object[0];
        jScrollPane.getViewport().add(mxgraphComponent, null);
        pack();
        setLocationRelativeTo(null);
        addVertex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addVertex();
            }
        });
        circumButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //backToIndex();
                if(verteces.length==0||mxgraph.getAllEdges(verteces).length==0)
                {
                    //JOptionPane.showMessageDialog(null, "Граф пуст! (необходимы связи или вершины для обхода в глубину!)");
                }
                else
                    Lab6Controller.circumvention(lab6Window, mxgraph, verteces);
            }
        });
        backToIndexesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                backToIndex();
            }
        });
        deleteVertexButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab6Window, 0);
            }
        });
        deleteEdgeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SupWindowForLab6 supWindowForLab6=new SupWindowForLab6(lab6Window, 1);
            }
        });
        int size=mxgraph.getAllEdges(verteces).length;
        /*Timer timer = new Timer(6000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (size != mxgraph.getAllEdges(verteces).length) {
                    if (verteces.length == 0 || mxgraph.getAllEdges(verteces).length == 0) {
                        //JOptionPane.showMessageDialog(null, "Граф пуст! (необходимы связи или вершины для обхода в глубину!)");
                    } else
                        Lab6Controller.circumvention(lab6Window, mxgraph, verteces);
                }
            }
        });
        timer.start();*/
    }
    public static Lab6Window getObject()
    {
        if(lab6Window==null)
        {
            lab6Window=new Lab6Window();
        }
        return lab6Window;
    }
    public void addVertex()
    {
        mxgraph.getModel().beginUpdate();
        try
        {
            Object v1 = mxgraph.insertVertex(parent, null, verteces.length+1, 20+countForPosition*80, 20, 50, 50);
            countForPosition++;
            Object[] newVerteces=new Object[verteces.length+1];
            for(int i=0;i<verteces.length;i++)
            {
                newVerteces[i]=verteces[i];
            }
            newVerteces[verteces.length]=v1;
            verteces=newVerteces;
        }
        finally
        {
            // Updates the display
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        circumButton.doClick();
    }
    public void setValue(String str)
    {
        //circumResult.setText("Результат обхода: "+str);
        mxgraph.getModel().beginUpdate();
        try
        {
            String [] arraystr=str.split(", ");
            int counter=arraystr.length;
            int secondCounter=1;
            for(int i=0;i<verteces.length;i++)
            {
                boolean change=true;
                for(int j=0;j<arraystr.length;j++) {
                    if (mxgraph.getModel().getValue(verteces[i]).equals(Integer.parseInt(arraystr[j])))
                    {
                        change=false;
                        break;
                    }
                }
                System.out.println(mxgraph.getModel().getValue(verteces[i]));
                if(change==true) {
                    mxgraph.getModel().setValue(verteces[i], counter+1);
                    counter++;
                }
                else
                {
                    mxgraph.getModel().setValue(verteces[i], secondCounter);
                    secondCounter++;
                }
            }
        }
        finally
        {
            // Updates the display
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        //DataBaseModel.setGraph(str);
    }
    public void removeVertex(int value)
    {
        if(verteces.length==0)
        {
            JOptionPane.showMessageDialog(null, "Вершин нет!");
            return;
        }
        if(value>verteces.length)
        {
            JOptionPane.showMessageDialog(null, "Данной вершины не существует!");
            return;
        }
        Object[] newVerteces=new Object[verteces.length-1];
        int add=0;
        for(int i=0;i<verteces.length;i++)
        {
            if(i!=(value-1)) {
                newVerteces[i-add] = verteces[i];
            }
            else
            {
                add++;
            }
        }
        mxgraph.getModel().beginUpdate();
        try {
            mxgraph.removeCells(new Object[]{verteces[value-1]});
            for(int i=0;i<newVerteces.length;i++)
            {
                mxgraph.getModel().setValue(newVerteces[i], i+1);
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        verteces=newVerteces;
        circumButton.doClick();
    }
    public void removeEdge(int firstVertex, int secondVertex)
    {
        try {
            if(mxgraph.getEdgesBetween(verteces[firstVertex-1], verteces[secondVertex-1], true).length!=0) {
                mxgraph.getModel().remove(mxgraph.getEdgesBetween(verteces[firstVertex - 1], verteces[secondVertex - 1], true)[0]);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Нет такой связи!");
                return;
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
        circumButton.doClick();
    }
    private void backToIndex()
    {
        try {
            for(int i=0;i<verteces.length;i++)
            {
                mxgraph.getModel().setValue(verteces[i], i+1);
            }
        } finally {
            mxgraph.getModel().endUpdate();
        }
        mxgraphComponent.updateComponents();
        jScrollPane.updateUI();
    }
}