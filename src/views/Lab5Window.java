package views;

import models.Model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;

public class Lab5Window extends JDialog {
    private JPanel contentPane;
    private JTextField arrayField;
    private JButton sortButton;
    private JTextArea resultArea;
    private static Lab5Window lab5Window;
    private Lab5Window() {
        setContentPane(contentPane);
        setTitle("Лабораторная №5");
        setModal(true);
        getRootPane().setDefaultButton(sortButton);
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        sortButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (arrayField.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Строка пуста!");
                    return;
                }
                else Model.simpleMerge(arrayField.getText(), lab5Window);
            }
        });
    }
    public static Lab5Window getObject()
    {
        if(lab5Window==null)
        {
            lab5Window=new Lab5Window();
        }
        return lab5Window;
    }

    public void setResultText(String strOut) {
        resultArea.setText(strOut);
        pack();
    }
}
