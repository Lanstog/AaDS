package views;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class MainWindow extends JDialog {
    private JPanel contentPane;
    private JPanel jButtonsPanel;
    private static MainWindow mainWindow;

    private MainWindow() {
        setTitle("Лабораторные работы");
        setContentPane(contentPane);
        setModal(true);
        createButtons();
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public static MainWindow getObject() {
        if (mainWindow == null)
            mainWindow = new MainWindow();
        return mainWindow;
    }

    private void createButtons() {
        File thisFolder = new File("./src/views");
        File[] files = thisFolder.listFiles();
        for (int i = 0; i < files.length; i++) {
            try {
                String str = files[i].getName().toString();
                String check = "Lab";
                String curClass = "controllers.Lab" + str.charAt(3) + "Controller";
                if (str.charAt(0) == check.charAt(0) && str.charAt(1) == check.charAt(1) && str.charAt(2) == check.charAt(2) && str.charAt(11) == 'j') {
                    JButton button = new JButton("Lab №" + str.charAt(3));
                    jButtonsPanel.add(button);
                    Class getClass = Class.forName(curClass);
                    Method method = getClass.getMethod("getObject");
                    button.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                method.invoke(getClass);
                            } catch (Exception error) {
                                JOptionPane.showMessageDialog(null, "Непредвиденная ошибка! (не удалось получить метод getObject)");
                            }
                        }
                    });
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Непредвиденная ошибка! (не удалось получить класс)");
            }
        }
    }
}

